---
title: Case studies from GitLab customers
description: Customers share how they've been able to shorten the software development lifecycle while using GitLab
blog_cards:
  - title: How UBS created their own DevOps platform using GitLab
    url: /blog/2021/08/04/ubs-gitlab-devops-platform/
    author: Sara Kassabian
    blog_img_url: /nuxt-images/blogimages/library/devops.png

  - title: How Orange made a first step toward CI/CD standardization with GitLab
    url: /blog/2021/07/29/how-orange-uses-gitlab-ci-cd-for-modern-devops/
    author: Pierre Smeyers
    blog_img_url: /nuxt-images/blogimages/oranges.jpg

  - title: How Zoopla used DORA metrics to boost deployments, increase automation and more
    url: /blog/2022/01/24/how-zoopla-uses-dora-metrics-and-your-team-can-too/
    author: Gustaw Fit of Zoopla
    blog_img_url: /nuxt-images/blogimages/blog-performance-metrics.jpg
callout_box:
  header: Register your interest in joining our Customer Reference Program
  text: |
    We are always looking for more customers to join our reference program so we can showcase their success stories.
  link_url: https://page.gitlab.com/reference.html
customers:
    - name: HackerOne
      label: hacker-one
      logo_img_url: /nuxt-images/logos/hackerone-logo.png
      headline: HackerOne achieves 5x faster deployments with GitLab’s integrated security
      button_text: Read Case Study
      button_url: /customers/hackerone/
      stats:
        - figure_number: 8 min
          figure_text: Pipeline time down from 60 (7.5x faster)
        - figure_number: 5x
          figure_text: Faster deployment time (4-5/day up from 1/day)
        - figure_number: 4 hours
          figure_text: Development time per engineer saved/weekly

    - name: The Zebra
      label: the-zebra
      logo_img_url: /nuxt-images/case-study-logos/thezebra_logo.png
      headline: How The Zebra achieved secure pipelines in black and white
      button_text: Read Case Study
      button_url: /customers/thezebra/
      stats:
        - figure_number: 50-75
          figure_text: Unknown vulnerabilities found using GitLab
        - figure_number: 1 tool
          figure_text: Down from 3 tools
        - figure_number: 2x
          figure_text: Deployment

    - name: Drupal Association
      label: drupal-association
      logo_img_url: /nuxt-images/customers/DA_stacked_blue_RGB.png
      headline: Drupal Association eases entry for new committers, speeds implementations
      button_text: Read Case Study
      button_url: /customers/drupalassociation/
      stats:
        - figure_number: 3x faster
          figure_text: Implementation of CI time reduced from 90 days to 30 days
        - figure_number: 6x rate
          figure_text: Frequency increased from 2-3 improvements released every 6 month, now improvements released monthly with every GitLab update
        - figure_number: 10% higher
          figure_text: Drupal core contributions up 10% since transitioning to GitLab Merge Requests

    - name: Siemens
      label: siemens
      logo_img_url: /nuxt-images/case-study-logos/siemens-logo.png
      headline: How Siemens created an open source DevOps culture with GitLab
      button_text: Read Case Study
      button_url: /customers/siemens/
      stats:
        - figure_number: 6.4M +
          figure_text: Builds per month (>210,000 builds per day)
        - figure_number: '0'
          figure_text: Support requests to GitLab
        - figure_number: 4+
          figure_text: Production deployments a month

    - name: Hilti
      label: hilti
      logo_img_url: /nuxt-images/case-study-logos/hilti-logo-1024x640.png
      headline: How CI/CD and robust security scanning accelerated Hilti’s SDLC
      button_text: Read Case Study
      button_url: /customers/hilti/
      stats:
        - figure_number: 400%
          figure_text: Increase in code checks (increase from 6 to 24 code checks every three months)
        - figure_number: 50%
          figure_text: Shortened feedback loops (feedback loops decreased from 6 days to 3 days)
        - figure_number: 15 min
          figure_text: Deployment time down from 3 hours (from 3 hours to just 15 minutes)

    - name: Fujitsu
      label: fujitsu
      logo_img_url: /nuxt-images/case-study-logos/FujitsuCTL_Logo.png
      headline: Fujitsu Cloud Technologies improves deployment velocity and cross-functional workflows with GitLab
      button_text: Read Case Study
      button_url: /customers/fujitsu/
      stats:
        - figure_number: '511'
          figure_text: users
        - figure_number: '526'
          figure_text: groups
        - figure_number: '5201'
          figure_text: projects
featured_studies_title: Leading companies around the world are choosing GitLab
case_studies:
  - title: HackerOne achieves 5x faster deployments with GitLab’s integrated security
    url: /customers/hackerone/
    img_url: /nuxt-images/blogimages/hackerone-cover-photo.jpg
    companySize: Mid-Market (101 - 2000)
    industry: Technology
    region: North America
    date_added: 2022-03-22

  - title: How The Zebra achieved secure pipelines in black and white
    url: /customers/thezebra/
    img_url: /nuxt-images/blogimages/thezebra_cover.jpg
    companySize: Mid-Market (101 - 2000)
    industry: Technology
    region: North America
    date_added: 2022-03-22


  - title: Drupal Association eases entry for new committers, speeds implementations
    url: /customers/drupalassociation/
    img_url: /nuxt-images/blogimages/drupalassoc_cover.jpg
    companySize: Mid-Market (101 - 2000)
    industry: Open Source Software
    region: North America
    date_added: 2022-03-22


  - title: Axway aims for elite DevOps status with GitLab
    url: /customers/axway-devops/
    img_url: /nuxt-images/blogimages/axway_casestudy_2.png
    companySize: Mid-Market (101 - 2000)
    industry: Technology
    region: North America
    date_added: 2022-03-22

  - title: Conversica leads AI innovation with help from GitLab Ultimate
    url: /customers/conversica/
    img_url: /nuxt-images/blogimages/conversicaimage.jpg
    companySize: Mid-Market (101 - 2000)
    industry: Technology
    region: North America
    date_added: 2022-03-22

  - title: How the U.S. Army Cyber School created “Courseware as Code” with GitLab
    url: /customers/us_army_cyber_school/
    img_url: /nuxt-images/blogimages/us-army-cyber-school.jpeg
    companySize: Mid-Market (101 - 2000)
    industry: Public Sector
    region: North America
    date_added: 2022-03-22

  - title: iFarm plants the seeds for operational efficiency
    url: /customers/ifarm/
    img_url: /nuxt-images/blogimages/ifarm-case-study-image.png
    companySize: Small to Medium Business (0 - 100)
    industry: Technology
    region: Latin America
    date_added: 2022-03-22

  - title: How Siemens created an open source DevOps culture with GitLab
    url: /customers/siemens/
    img_url: /nuxt-images/blogimages/siemenscoverimage_casestudy.jpg
    companySize: Enterprise (2001+)
    industry: Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-22

  - title: How CI/CD and robust security scanning accelerated Hilti’s SDLC
    url: /customers/hilti/
    img_url: /nuxt-images/blogimages/hilti_cover_image.jpg
    companySize: Enterprise (2001+)
    industry: Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-22

  - title: Moneyfarm deploys faster using fewer tools with GitLab
    url: /customers/moneyfarm/
    img_url: /nuxt-images/blogimages/moneyfarm_cover_image_july.jpg
    companySize: Mid-Market (101 - 2000)
    industry: Financial Services
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-22

  - title: How Crédit Agricole Corporate & Investment Bank (CACIB) transformed its global workflow with GitLab
    url: /customers/credit-agricole/
    img_url: /nuxt-images/blogimages/creditagricole-cover-image.jpg
    companySize: Enterprise (2001+)
    industry: Financial Services
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-22

  - title: Keytrade Bank centralizes its tooling around GitLab
    url: /customers/keytradebank/
    img_url: /nuxt-images/blogimages/keytradebank_cover.jpg
    companySize: Enterprise (2001+)
    industry: Financial Services
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-22

  - title: Radio France deploys 5x faster with GitLab CI/CD
    url: /customers/radiofrance/
    img_url: /nuxt-images/blogimages/radio-france-cover-image.jpg
    companySize: Enterprise (2001+)
    industry: Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-22

  - title: How Kiwi.com transformed its workflow with GitLab and Docker
    url: /customers/kiwi/
    img_url: /nuxt-images/blogimages/kiwi-cover.jpg
    companySize: Enterprise (2001+)
    industry: Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-22

  - title: Learn how GitLab is accelerating DevOps at Bendigo and Adelaide Bank
    url: /customers/bab/
    img_url: /nuxt-images/blogimages/bab_cover_image.jpg
    companySize: Enterprise (2001+)
    industry: Financial Services
    region: Asia Pacific
    date_added: 2022-03-22

  - title: Fujitsu Cloud Technologies improves deployment velocity and cross-functional workflows with GitLab
    url: /customers/fujitsu/
    img_url: /nuxt-images/blogimages/fjct_cover.jpg
    companySize: Mid-Market (101 - 2000)
    industry: Technology
    region: Asia Pacific
    date_added: 2022-03-22

  - title: Increasing development speed to keep students in school
    url: /customers/EAB/
    img_url: /nuxt-images/blogimages/eab_case_study_Image.jpg
    companySize:  Mid-Market (101 - 2000)
    industry: Education
    region:  North America
    date_added: 2022-03-21

  - title: How GitLab Geo supports NVIDIA’s innovation
    url: /customers/Nvidia/
    img_url: /nuxt-images/blogimages/nvidia.jpg
    companySize: Enterprise (2001+)
    industry: Technology
    region: North America
    date_added: 2022-03-21

  - title: How Zoopla deploys 700% faster with GitLab
    url: /customers/zoopla/
    img_url: /nuxt-images/blogimages/zoopla_cover_image.jpg
    companySize: Mid-Market (101 - 2000)
    industry: Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: The University of Surrey achieves top marks for collaboration and workflow management with GitLab
    url: /customers/university-of-surrey/
    img_url: /nuxt-images/blogimages/university_of_surrey_cover_image.jpg
    companySize: Enterprise (2001+)
    industry: Education
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: How Parts Unlimited quadrupled stakeholder value using GitLab
    url: /customers/parts_unlimited/
    img_url: /nuxt-images/blogimages/partsunlimited.jpg
    companySize: Enterprise (2001+)
    industry: Retail
    region: North America
    date_added: 2022-03-21

  - title: Airbus takes flight with GitLab releasing features 144x faster
    url: /customers/airbus/
    img_url: /nuxt-images/blogimages/airbus_cover_image.jpg
    companySize: Enterprise (2001+)
    industry: Science & Research
    region: North America
    date_added: 2022-03-21

  - title: Glympse is making geo-location sharing easy
    url: /customers/glympse/
    img_url: /nuxt-images/blogimages/glympse_case_study.jpg
    companySize: Small to Medium Business (0 - 100)
    industry: Technology
    region: North America
    date_added: 2022-03-21

  - title: How X-Cite uses GitLab’s SCM for robust worldwide workflow
    url: /customers/xcite/
    img_url: /nuxt-images/blogimages/xcite_cover_image.jpg
    companySize: Small to Medium Business (0 - 100)
    industry: Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: ExtraHop fully embraces CI/CD process transformation with GitLab
    url: /customers/extra-hop-networks/
    img_url: /nuxt-images/blogimages/shahadat-rahman-gnyA8vd3Otc-unsplash.jpg
    companySize: Mid-Market (101 - 2000)
    industry: Technology
    region: North America
    date_added: 2022-03-21

  - title: How GitLab CI/CD supports and accelerates innovation for Anchormen
    url: /customers/anchormen/
    img_url: /nuxt-images/blogimages/anchormen.jpg
    companySize: Small to Medium Business (0 - 100)
    industry: Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Particle physics laboratory uses GitLab to connect researchers from across the globe
    url: /customers/cern/
    img_url: /nuxt-images/blogimages/cern.jpg
    companySize: Enterprise (2001+)
    industry: Science & Research
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Goldman Sachs improves from 1 build every two weeks to over a thousand per day
    url: /customers/goldman-sachs
    img_url: /nuxt-images/blogimages/Goldman_Sachs_case_study.jpg
    companySize: Enterprise (2001+)
    industry: Financial Services
    region: North America
    date_added: 2022-03-21

  - title: How Remote meets 100% of deadlines with GitLab
    url: /customers/remote
    img_url: /nuxt-images/blogimages/remote.jpg
    companySize: Small to Medium Business (0 - 100)
    industry: Technology
    region: Remote
    date_added: 2022-03-21

  - title: How Dublin City University empowers students for the IT industry with GitLab
    url: /customers/dublin-city-university
    img_url: /nuxt-images/blogimages/dcu.jpg
    companySize: Mid-Market (101 - 2000)
    industry: Education
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: How MGA builds projects 5 times faster with GitLab
    url: /customers/mga
    img_url: /nuxt-images/blogimages/covermga.jpg
    companySize: Small to Medium Business (0 - 100)
    industry: Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Removing multi-tool barriers to achieve 14 builds a day
    url: /customers/alteryx
    img_url: /nuxt-images/blogimages/alteryx_case_study.jpg
    companySize: Mid-Market (101 - 2000)
    industry: Technology
    region: North America
    date_added: 2022-03-21

  - title: How Hotjar deploys 50% faster with GitLab
    url: /customers/hotjar
    img_url: /nuxt-images/blogimages/hotjar.jpg
    companySize: Small to Medium Business (0 - 100)
    industry: Technology
    region: Remote
    date_added: 2022-03-21

  - title: How Parimatch scores big with GitLab
    url: /customers/parimatch
    img_url: /nuxt-images/blogimages/pmbet_cover.jpg
    companySize: Mid-Market (101 - 2000)
    industry: Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Worldline improves code reviews’ potential by 120x
    url: /customers/worldline
    img_url: /nuxt-images/blogimages/worldline-case-study-image.jpg
    companySize: Enterprise (2001+)
    industry: Financial Services
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: How Nebulaworks replaced 3 tools with GitLab and empowered customer speed and agility
    url: /customers/nebulaworks
    img_url: /nuxt-images/blogimages/nebulaworks.jpg
    companySize: Small to Medium Business (0 - 100)
    industry: Technology
    region: North America
    date_added: 2022-03-21

  - title: How Chicago’s Cook County assesses economic data with transparency and version control
    url: /customers/cook-county
    img_url: /nuxt-images/blogimages/cookcounty.jpg
    companySize: Mid-Market (101 - 2000)
    industry: Public Sector
    region: North America
    date_added: 2022-03-21

  - title: The Paul G. Allen Center for Computer Science & Engineering gains control and flexibility to easily manage 10,000+ projects.
    url: /customers/uw
    img_url: /nuxt-images/blogimages/uw-case-study-image.png
    companySize: Mid-Market (101 - 2000)
    industry: Public Sector
    region: North America
    date_added: 2022-03-21

  - title: Trek10 provides radical visibility to clients
    url: /customers/trek10
    img_url: /nuxt-images/blogimages/trek10-bg.png
    companySize: Small to Medium Business (0 - 100)
    industry: Technology
    region: North America
    date_added: 2022-03-21

  - title: How GitLab decreased deployment times from 2 days to just 5 minutes for Inventx AG
    url: /customers/inventx
    img_url: /nuxt-images/blogimages/cover_image_inventxag_case_study.jpg
    companySize: Mid-Market (101 - 2000)
    industry: Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Equinix increases the agility of their DevOps teams with self-serviceability and automation.
    url: /customers/equinix
    img_url: /nuxt-images/blogimages/equinix-case-study-image.png
    companySize: Enterprise (2001+)
    industry:  Technology
    region: North America
    date_added: 2022-03-21

  - title: How British Geological Survey revolutionized its software development lifecycle
    url: /customers/bgs
    img_url: /nuxt-images/blogimages/bgs-cover.jpg
    companySize: Mid-Market (101 - 2000)
    industry: Science & Research
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: GitLab delivers faster pipeline builds and improved code quality for Weave
    url: /customers/weave
    img_url: /nuxt-images/blogimages/weave_cover_image.jpg
    companySize: Small to Medium Business (0 - 100)
    industry: Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Signicat reduces deploy time from days to just minutes
    url: /customers/signicat
    img_url: /nuxt-images/blogimages/signicat_cover_image.jpg
    companySize: Mid-Market (101 - 2000)
    industry: Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Paessler AG switched from Jenkins to GitLab and ramped up to 4x more releases
    url: /customers/paessler
    img_url: /nuxt-images/blogimages/paessler-case-study-image.png
    companySize: Mid-Market (101 - 2000)
    industry: Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: How Paessler deploys up to 50 times daily with GitLab Premium
    url: /customers/paessler-prtg
    img_url: /nuxt-images/blogimages/paessler.jpg
    companySize: Mid-Market (101 - 2000)
    industry: Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: From Bicycles to Connected Driving
    url: /customers/anwb
    img_url: /nuxt-images/blogimages/anwb_case_study_image_2.jpg
    companySize: Enterprise (2001+)
    industry: Retail
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: The Future is Conversation Intelligence
    url: /customers/chorus
    img_url: /nuxt-images/blogimages/Chorus_case_study.png
    companySize: Mid-Market (101 - 2000)
    industry: Technology
    region: North America
    date_added: 2022-03-21

  - title: GitLab enables the VLabsDev community support excellence
    url: /customers/virtual_labs_dev
    img_url: /nuxt-images/blogimages/vlabsdev_coverimage.jpg
    companySize: Enterprise (2001+)
    industry: Education
    region: Asia Pacific
    date_added: 2022-03-21

  - title: GitLab advances open science education at Te Herenga Waka – Victoria University of Wellington
    url: /customers/victoria_university
    img_url: /nuxt-images/blogimages/vic_uni_cover_image.jpg
    companySize: Enterprise (2001+)
    industry: Education
    region: Asia Pacific
    date_added: 2022-03-21

  - title: Increasing deployments to 10 times a day
    url: /customers/bi_worldwide
    img_url: /nuxt-images/blogimages/bi_worldwise_casestudy_image.png
    companySize: Enterprise (2001+)
    industry: Consulting
    region: North America
    date_added: 2022-03-21

  - title: How Lely replaced three tools with GitLab Self Managed Premium to maximize efficiency
    url: /customers/lely
    img_url: /nuxt-images/blogimages/lelyservicesoption2.jpg
    companySize: Mid-Market (101 - 2000)
    industry: Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Hemmersbach reorganized their build chain and increased build speed 59x
    url: /customers/hemmersbach
    img_url: /nuxt-images/blogimages/hemmersbach_case_study.jpg
    companySize: Enterprise (2001+)
    industry: Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: GitLab offers Fanatics the CI stability they were searching for
    url: /customers/fanatics
    img_url: /nuxt-images/blogimages/fanatics_case_study_image.png
    companySize: Mid-Market (101 - 2000)
    industry: Retail
    region: North America
    date_added: 2022-03-21

  - title: Connecting the cosmos with Earth - How the European Space Agency uses GitLab to focus on space missions
    url: /customers/european-space-agency
    img_url: /nuxt-images/blogimages/ESA_case_study_image.jpg
    companySize: Enterprise (2001+)
    industry: Science & Research
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Fintech innovator Curve counts on the GitLab platform
    url: /customers/curve
    img_url: /nuxt-images/blogimages/curve_cover_image.jpg
    companySize: Mid-Market (101 - 2000)
    industry: Europe, the Middle East, and Africa
    region: Financial Services
    date_added: 2022-03-21

  - title: How Potato uses GitLab CI for cutting edge innovation
    url: /customers/potato-london
    img_url: /nuxt-images/blogimages/potato-london.jpg
    companySize: Small to Medium Business (0 - 100)
    industry: Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: How GitLab became the cornerstone of digital enablement for Sopra Steria
    url: /customers/sopra_steria
    img_url: /nuxt-images/blogimages/soprasteria.jpg
    companySize: Enterprise (2001+)
    industry: Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: How Veepee accelerated deployment from 4 days to 4 minutes
    url: /customers/veepee
    img_url: /nuxt-images/blogimages/veepee.jpg
    companySize: Enterprise (2001+)
    industry: Retail
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: GitLab CI keeps financial institution secure and scalable
    url: /customers/financial-company
    img_url: /nuxt-images/blogimages/financial_company_case_study_image.png
    companySize:
    industry: Financial Services
    region: North America
    date_added: 2022-03-21

  - title: Learn how EveryMatrix wins big with GitLab
    url: /customers/everymatrix
    img_url: /nuxt-images/blogimages/everymatrix_cover_image.jpg
    companySize: Mid-Market (101 - 2000)
    industry: Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Axway realizes a 26x faster release cycle by switching from Subversion to GitLab
    url: /customers/axway
    img_url: /nuxt-images/blogimages/axway-case-study-image.png
    companySize: Enterprise (2001+)
    industry: Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: How Cloud Native Computing Foundation eliminates the complexity of managing multiple projects across many cloud providers
    url: /customers/cncf
    img_url: /nuxt-images/blogimages/cncf-case-study-image.png
    companySize: Mid-Market (101 - 2000)
    industry: Technology
    region: North America
    date_added: 2022-03-21

  - title: How SVA enhanced business agility workflow with GitLab
    url: /customers/sva
    img_url: /nuxt-images/blogimages/sva.jpg
    companySize: Mid-Market (101 - 2000)
    industry: Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: How REGENHU reaps 50% YoY in cost savings annually with GitLab
    url: /customers/regenhu
    img_url: /nuxt-images/blogimages/cover_image_regenhu.jpg
    companySize: Small to Medium Business (0 - 100)
    industry: Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: How New10 deploys 3 times faster with GitLab
    url: /customers/new10
    img_url: /nuxt-images/blogimages/new-ten-euro.jpg
    companySize: Small to Medium Business (0 - 100)
    industry: Financial Services
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Security provider KnowBe4 keeps code in-house and speeds up deployment
    url: /customers/knowbe4
    img_url: /nuxt-images/blogimages/knowbe4cover-sm.jpg
    companySize: Mid-Market (101 - 2000)
    industry: Technology
    region: North America
    date_added: 2022-03-21

  - title: GitLab places Ibeo Automotive Systems in pole position for driving innovation and scaling efficiencies
    url: /customers/ibeo_automotive
    img_url: /nuxt-images/blogimages/cover_image_ibeo_auto.jpg
    companySize: Mid-Market (101 - 2000)
    industry: Automotive
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Chefkoch improved project visibility and deploys 40% faster
    url: /customers/chefkoch
    img_url: /nuxt-images/blogimages/chefkock_cover_image.jpg
    companySize: Small to Medium Business (0 - 100)
    industry: Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Glispa successfully migrated to Kubernetes in 3 months with GitLab
    url: /customers/glispa
    img_url: /nuxt-images/blogimages/glispa.jpg
    companySize: Mid-Market (101 - 2000)
    industry: Retail
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: How Jasper Solutions offers “DevSecOps in a box” with GitLab
    url: /customers/jasper-solutions
    img_url: /nuxt-images/blogimages/jasper.jpg
    companySize: Small to Medium Business (0 - 100)
    industry: Technology
    region: North America
    date_added: 2022-03-21

  - title: How GitLab automates course work, enhances coding feedback, advances software quality, and scales for success.
    url: /customers/heriot_watt_university
    img_url: /nuxt-images/blogimages/cover_image_heriot_watt_uni.jpg
    companySize: Enterprise (2001+)
    industry: Europe, the Middle East, and Africa
    region: Education
    date_added: 2022-03-21

  - title: How SKA uses GitLab to help construct the world’s largest telescope
    url: /customers/square_kilometre_array
    img_url: /nuxt-images/blogimages/ska-cover.jpg
    companySize: Small to Medium Business (0 - 100)
    industry: Science & Research
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Hemmersbach reorganized their build chain and increased build speed 59x
    url: /customers/hemmersbach
    img_url: /nuxt-images/blogimages/hemmersbach_case_study.jpg
    companySize: Enterprise (2001+)
    industry: Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: How SURF increased deployment speed by 1,400%
    url: /customers/surf
    img_url: /nuxt-images/blogimages/surf.jpg
    companySize: Mid-Market (101 - 2000)
    industry: Science & Research
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Global nonprofit OW2 uses GitLab to help the developer community create software more efficiently
    url: /customers/ow2
    img_url: /nuxt-images/blogimages/ow2-case-study-image.png
    companySize: Small to Medium Business (0 - 100)
    industry: Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21
quotes_block: 
  header: Teams do more with GitLab
  no_background: true
  quotes:
    - title_img:
        url: /nuxt-images/case-study-logos/siemens-logo.png
        alt: Siemens Logo
      quote: We really try to bring the open source culture in, and so far, we really succeeded. With CI/CD, we have one and a half million builds every month. The whole culture has completely changed.
      author: Fabio Huser, Software Architect at Siemens Smart Infrastructure
      ga_carousel: Siemens case study
      url: '/customers/siemens'
