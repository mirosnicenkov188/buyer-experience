---
  title: GitLab for Education Survey Report
  description: Read the results of our GitLab for Education Survey.
  twitter_image: /nuxt-images/open-graph/education-survey-2020-opengraph.png
  components:
    - name: 'solutions-hero'
      data:
        title: GitLab for Education 2020 Program Survey
        subtitle: See the results and discover how DevOps is transforming education
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          url: /solutions/education/edu-survey/edu-survey-2020.pdf
          text: Download the report now
          data_ga_name: education survey report
          data_ga_location: header
        image:
          image_url: /nuxt-images/solutions/education.jpeg
          alt: "Image: gitlab for education"
          rounded: true
    - name: edu-survey-copy-media
      data:
        block:
          - header: Download the GitLab for Education 2020 Program Survey Results
            aos_animation: fade-up
            aos_duration: 500
            text: |
              * Discover how GitLab's Education Program Members are transforming education with DevOps
              * Understand the advantages of bringing DevOps & GitLab to your classroom
              * Learn how DevOp is innovating scientific research
              * See what challenges educational institutions face in their DevOps journey
            image:
              image_url: /nuxt-images/community/edu-survey-2020-cover.png
              alt: ""
            links:
              - text: Download the report now
                href: /solutions/education/edu-survey/edu-survey-2020.pdf
                data_ga_name: education survey report
                data_ga_location: body
              - text: Learn more about the GitLab for Education Program
                href: /solutions/education
                data_ga_name: education program
                data_ga_location: body
    - name: featured-media
      data:
        column_size: 6
        header: Key Takeaways
        header_animation: fade-up
        header_animation_duration: 500
        with_background: true
        media:
          - title: Institution-wide adoption
            aos_animation: fade-up
            aos_duration: 500
            text: |
              GitLab is used extensively across the entire educational institution. Adoption extends well beyond typical Computer Science departments into many academic disciplines and colleges on campuses, including engineering, natural and social sciences, medical fields, and library science.
          - title: DevOps in the classroom
            aos_animation: fade-up
            aos_duration: 500
            text: |
              GitLab for Education Program members teach and learn every stage of the DevOps lifecycle. The ability to teach all stages in one platform is a crucial advantage for both the faculty and students. Adoption is higher in the manage through configure stages, with the least adoption in the secure and protect stages.
          - title: Multi-disciplinary adoption
            aos_animation: fade-up
            aos_duration: 500
            text: |
              GitLab is used for a wide variety of purposes across the institution and generally for more than one primary purpose. Uses include teaching, learning, research, student portfolios, information technology, campus administration, medical and library sciences, and building open source software.
          - title: Cross campus collaboration
            aos_animation: fade-up
            aos_duration: 500
            text: |
              DevOps and GitLab are not siloed into either teaching code or running the institution. Cross-collaboration and multiple use cases between students, faculty, staff, and information technology professionals are common. DevOps has become an enterprise tool at educational institutions.
    - name: pull-quote
      data:
        aos_animation: zoom-in-left
        aos_duration: 800
        quote: |
          I've found the GitLab for Education Program to be invaluable for both teaching and research. The student cohorts receive in-depth training on how to use GitLab, as an exemplar of contemporary DevOps practice.
        shadow: true
        hide_horizontal_rule: true
    - name: featured-media
      data:
        column_size: 6
        with_background: true
        media:
          - title: GitLab's deployment flexibility
            aos_animation: fade-up
            aos_duration: 500
            text: |
              The flexibility of deployment method, either self-managed or SaaS, is a large advantage of the GitLab for Education Program offering. Campuses are able to choose the method that works best with their security requirements and authentication systems as well as meeting research funding requirements.
          - title: Need for increased awareness outside of early adopters
            aos_animation: fade-up
            aos_duration: 500
            text: |
              The survey highlights a need to increase awareness of DevOps and GitLab for Education offerings among faculty.
          - title: Advancing and transforming science
            aos_animation: fade-up
            aos_duration: 500
            text: |
              GitLab transforms and advances scientific research. Source control management, continuous integration, and continuous deployment are adopted to increase collaboration, speed up the research cycle, increase the repeatability of results, and meet public access policy requirements of funding agencies.
    - name: pull-quote
      data:
        aos_animation: zoom-in-right
        aos_duration: 800
        quote: |
          The students nor faculty realize how GitLab could impact their daily life in terms of project collaboration even if they know it exists.
        shadow: true
        hide_horizontal_rule: true
